@extends('layout.master')

@section('judul')
    Halaman Index
@endsection

@section('content')
    
<a href="/film/create" class="btn btn-success mb-3">Tambah</a>

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Ringkasan</th>
                <th scope="col">Poster</th>
                <th scope="col">Genre Id</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($film as $key => $item)
                <tr>
                    <td>{{ $key + 1}}</td>
                    <td>{{ $item->judul }}</td>
                    <td>{{ $item->ringkasan }}</td>
                    <td>{{ $item->tahun }}</td>
                    <td>{{ $item->poster }}</td>
                    <td>{{ $item->genre_id }}</td>
                    <td>
                        
                        <form action="/film/{{$item->id}}" method="POST">
                            @method('delete')
                            @csrf

                            <a href="/film/{{$item->id}}" class="btn btn-secondary btn-sm">Detail</a>
                            <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                </tr>
            @empty
                <tr>
                    <td>Data Masih Kosong</td>
                </tr>
            @endforelse
        </tbody>
    </table>

@endsection

   
    
    


