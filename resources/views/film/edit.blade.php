@extends('layout.master')

@section('judul')
    Halaman Edit Film {{$film->judul}}
@endsection

@section('content')
    
    <form action="/film/{{$film->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label >Nama Judul</label>
        <input type="text" class="form-control" name="judul" value="{{$film->judul}}">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Ringkasan</label>
        <input type="text" class="form-control" name="ringkasan" value="{{$film->ringkasan}}">
        
    </div>
    @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Tahun</label>
        <input type="text" class="form-control" name="tahun" value="{{$film->tahun}}">
        
    </div>
    @error('tahun')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Poster</label>
        <input type="text" class="form-control" name="poster" value="{{$film->poster}}">
        
    </div>
    @error('poster')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Genre Id</label>
        <input type="text" class="form-control" name="genre_id" value="{{$film->genre_id}}">
        
    </div>
    @error('genre_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection

   
