<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    //
    public function index()
    {
        $film = DB::table('film')->get();
        return view('film.index', compact('film'));
    }

    public function create()
    {
        return view('film.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => 'required'
        ]);

        DB::table('film')->insert([
            'judul' => $request['judul'],
            'ringkasan' => $request['ringkasan'],            
            'tahun' => $request['tahun'],
            'poster' => $request['poster'],
            'genre_id' => $request['genre_id']
        ]);

        return redirect('/film');
    }

    public function show($id)
    {
        $film = DB::table('film')->where('id', $id)->first();
        return view('film.show', compact('film'));
    }

    public function edit($id)
    {
        $film = DB::table('film')->where('id', $id)->first();
        return view('film.edit', compact('film'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => 'required'
        ]);

        $query = DB::table('film')
                ->where('id', $id)
                ->update([
                    'judul' => $request['judul'],
                    'ringkasan' => $request['ringkasan'],            
                    'tahun' => $request['tahun'],
                    'poster' => $request['poster'],
                    'genre_id' => $request['genre_id']
                ]);

        return redirect('/film');
    }

    public function destroy($id)
    {
        DB::table('film')->where('id', $id)->delete();
        return redirect('/film');
    }
}
